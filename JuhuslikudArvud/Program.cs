﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuhuslikudArvud
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random(5138100);

            // Mõtlemise ülesanne
            // sega arvud 0..51 ära

            List<int> numbrid = new List<int>(Enumerable.Range(0, 52));
            Console.WriteLine("enne segamist: ");
            foreach (var x in numbrid) Console.Write($"{x:00} ");
            List<int> segatud = new List<int>();

            while(numbrid.Count > 0)
            {
                int mitmes = r.Next() % numbrid.Count();
                segatud.Add(numbrid[mitmes]);
                numbrid.RemoveAt(mitmes);
            }
            Console.WriteLine("segatud: ");
            foreach (var x in segatud) Console.Write($"{x:00} ");
            Console.WriteLine();

            Console.WriteLine("tagasi: ");
            foreach (var x in segatud.OrderBy(x=>x)) Console.Write($"{x:00} ");
            Console.WriteLine();



        }
    }
}
